﻿using System;

namespace Task__1
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Привет, введи своё имя");
                var name = Console.ReadLine();
                Console.WriteLine($"Рад с тобой познакомиться: {name}");

                Console.WriteLine("Введи свой возраст");
                var age = int.Parse(Console.ReadLine());
                Console.WriteLine($"Ого, тебе: {age}");

                Console.WriteLine("Введи цифру от 3 до 9");
                var number = int.Parse(Console.ReadLine());
                if (number >= 3 && number <= 9)
                {
                    Console.WriteLine($"Твоя цифра: {number}");

                    Console.WriteLine($"{name}, я проанализировал введеные тобой данные и могу заявить: ");
                    Console.WriteLine($"1) Твой возраст - это {CheckEvenNumber(age)}");
                    Console.Write($"2) Квадратный корень, полученный из твоего возраста, ");
                    Console.Write($"{CheckDivisibilityNumber(age, number)} на {number}");
                }
                else
                {
                    Console.WriteLine("Недопустимая цифра");
                }

            }
            catch
            {
                Console.WriteLine("Вы ввели недопустимые данные");
            }
            Console.ReadKey();
        }

        public static string CheckEvenNumber(int number)
        {
            var result = (number % 2 == 0)
                ? "Четное число"
                : "Нечетное число";

            return result;
        }
        public static string CheckDivisibilityNumber(int number, int divisor)
        {
            var result = (Math.Sqrt(number) % divisor == 0)
                ? "делится"
                : "не делится";

            return result;
        }
    }
}
